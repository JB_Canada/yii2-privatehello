Private Test
============
This is a test of the private Extension

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist jberall/yii2-privatehello "*"
```

or add

```
"jberall/yii2-privatehello": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \jberall\privatehello\AutoloadExample::widget(); ?>```